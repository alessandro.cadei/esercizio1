# main.py
from fastapi import FastAPI
from src.model import prepare_iris_dataset, train_random_forest, make_evaluation, predict
from pydantic import BaseModel

app = FastAPI()


class Dataframe(BaseModel):
    df: dict  # trovare il modo di descrivere la struttura di questo dict per la documentazione


class IrisValues(BaseModel):
    model_filename: str
    values: list[float]


@app.get("/")
async def root():
    return {"greeting":"Hello world"}


@app.post("/train_and_evaluate")
async def train_and_evaluate(input: Dataframe):

    '''Prepares a trained RandomForest classifier on Iris dataset and evaluate it with logloss and accuracy'''

    X_train, X_test, y_train, y_test = prepare_iris_dataset(df=input.df, seed=37)
    model, filename = train_random_forest(X_train=X_train, y_train=y_train)
    results = make_evaluation(model=model, X_test=X_test, y_test=y_test)
    return {'results': results, 'filename': filename}


@app.post("/prediction")
async def prediction(input: IrisValues):

    '''Performs a prediction on Iris dataset using a pretrained RandomForest classifier using the supplied values'''
    
    predicted_class = predict(filename=input.model_filename, values=input.values)
    
    return {'class': predicted_class}

