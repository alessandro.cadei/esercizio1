from sklearn.model_selection import train_test_split
from sklearn.metrics import log_loss, accuracy_score
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
import numpy as np
import pickle5 as pickle
import os

MODEL_NAME = 'random_forest.sav'

def prepare_iris_dataset(df, seed):

    if type(df) == pd.core.frame.DataFrame:
        iris = df.copy()
    else:
        iris = pd.DataFrame(data=df)
    iris.sample(frac=1, random_state=seed)
    X = iris.drop(labels=['Species', 'Id'], axis=1)
    y = iris[['Species']]
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=seed, stratify=y)

    return X_train, X_test, y_train, y_test


def train_random_forest(X_train, y_train):

    '''Train a random forest classifier and save it to a file for reuse. If a trained model already exists it loads such model'''

    base_folder = './models'
    filename = f'{base_folder}/{MODEL_NAME}'

    if not os.path.exists(base_folder):
        os.makedirs('./models')

    if not os.path.exists(filename):
        model = RandomForestClassifier(n_estimators=100)

        # Train the model
        model.fit(X_train, y_train)
        
        #  Save trained model to file
        with open(filename, 'wb') as written:
            pickle.dump(model, written)
    else:
        # Read pretrained model from file
        with open(filename, 'rb') as read:
            model = pickle.load(read)
   
    return model, filename


def make_evaluation(model, X_test, y_test):

    '''Evaluetion performed by computing log_loss and accuracy'''

    y_pred_proba = model.predict_proba(X_test)
    y_pred = model.predict(X_test)

    ll = log_loss(y_test, y_pred_proba)
    acc = accuracy_score(y_test, y_pred)

    return {'log_loss': ll, 'accuracy': acc}


def predict(filename, values: list):

    '''Performs a single prediction with the given model and the given data'''

    if not os.path.exists(filename):
        return 'Pre-trained model does not exists; train one before'

    with open(filename, 'rb') as model_source:
        model = pickle.load(model_source)

    vals = np.array(values).reshape(1, -1)
    predicted_class = model.predict(vals)[0]

    return predicted_class
