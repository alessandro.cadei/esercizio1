import streamlit as st
import pandas as pd
import requests
import json
from model import prepare_iris_dataset, train_random_forest, make_evaluation

# Streamlit ha problemi con python3.9.7

pages = ['Streamlit pred', 'FastApi pred']

# capire o chiedere perchè mi potrebbe servire questo
if not hasattr(st, 'some_attribute'):
    st.some_attribute = None

selectbox = st.sidebar.selectbox(
    "What do you want to do?",
    pages,
    format_func=lambda p: p
)

def upload_df_file():

    uploaded_file = st.file_uploader("Choose a file")

    if uploaded_file is not None:
        dataframe = pd.read_csv(uploaded_file)
        return dataframe
    else:
        return None
    
def build_sliders_and_get_values(df):

    features_max_min = {'SepalLenghtCm':
                    {'min': float(df['SepalLengthCm'].min()),
                     'max': float(df['SepalLengthCm'].max())},
                'SepalWidthCm':
                    {'min': float(df['SepalWidthCm'].min()),
                     'max': float(df['SepalWidthCm'].max())},
                'PetalLengthCm':
                    {'min': float(df['PetalLengthCm'].min()),
                     'max': float(df['PetalLengthCm'].max())},
                'PetalWidthCm':
                    {'min': float(df['PetalWidthCm'].min()),
                     'max': float(df['PetalWidthCm'].max())},}
    sl = st.slider(label='SepalLengthCm', min_value=features_max_min['SepalLenghtCm']['min'], max_value=features_max_min['SepalLenghtCm']['max'])
    sw = st.slider(label='SepalWidthCm', min_value=features_max_min['SepalWidthCm']['min'], max_value=features_max_min['SepalWidthCm']['max'])
    pl = st.slider(label='PetalLengthCm', min_value=features_max_min['PetalLengthCm']['min'], max_value=features_max_min['PetalLengthCm']['max'])
    pw = st.slider(label='PetalWidthCm', min_value=features_max_min['PetalWidthCm']['min'], max_value=features_max_min['PetalWidthCm']['max'])
    
    values = [sl, sw, pl, pw]

    return values

# Pages
if selectbox == pages[0]:  # evaluates a prediction by directly accessing model class
    st.title('Streamlit')
    df = upload_df_file()
    # values = build_sliders_and_get_values(df)

    if df is not None:
        X_train, X_test, y_train, y_test = prepare_iris_dataset(df=df, seed=37)
        model, filename = train_random_forest(X_train=X_train, y_train=y_train)
        metrics = make_evaluation(model=model, X_test=X_test, y_test=y_test)
        st.write('Evaluation of classification with RandomForest classifier')
        st.write(f"log_loss: {round(metrics['log_loss'], 4)}")
        st.write(f"accuracy: {round(metrics['accuracy']*100, 2)}%")
   
elif selectbox == pages[1]:  # evaluates a prediction by requesting the result to the FastAPI server
    st.title('FastApi')
    df = upload_df_file()
    if df is not None:
        values = build_sliders_and_get_values(df)
        dict_df = df.to_dict(orient='list')  # do this because cannot json serialize a pandas dataframe

        # train and evaluate the model
        resp1 = requests.post(url='http://fastapi:8886/train_and_evaluate', data=json.dumps({'df': dict_df}))  
        jresp1 = resp1.json()
        model_filename = jresp1['filename']

        # perform a single prediction using sliders' values
        resp2 = requests.post(url='http://fastapi:8886/prediction', data=json.dumps({'model_filename': model_filename, 'values': values}))
        result_class = resp2.json()['class']
        st.header(f':blue[**{result_class}**]')
    