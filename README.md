## Setup environment
Create and activate virtual environment (linux):  

```
python -m venv <venv_name>
source <venv_name>/bin/activate
```

Streamlit is likely to fail with python3.9.7.
Update python version to any version above 3.9.7 to fix.

## Launch with Docker
With Docker both Streamlit and FastAPI will be lauched.

```
docker-compose up [--build]
```

## Launch Streamlit

```
streamlit run src/main.py
```

## Launch FastAPI server

```
uvicorn src.main2:app --reload
uvicorn src.main2:app --reload --host 0.0.0.0 --port 8886
```

## Launch tests

```
docker-compose run fastapi pytest
```

# Notes

When creating or updating requirements.txt make sure to change "sklearn" into "scikit-learn"

## References
* [venv creation](https://docs.python.org/3/library/venv.html)
* [streamlit](https://docs.streamlit.io/library/get-started/create-an-app): Create an app in streamlit
* [FastAPI](https://kinsta.com/it/blog/fastapi/): Build an app with FastAPI in python 
* [Docker installation](https://docs.docker.com/engine/install/ubuntu/)
* [Docker guide](https://medium.com/@kmdkhadeer/docker-get-started-9aa7ee662cea): Learn Docker
* [Docker compose](https://docs.docker.com/compose/install/linux/)
* [Streamlit+FastAPI](https://medium.com/codex/streamlit-fastapi-%EF%B8%8F-the-ingredients-you-need-for-your-next-data-science-recipe-ffbeb5f76a92)
