import imp
from fastapi.testclient import TestClient
from unittest import mock
import pandas as pd
from random import random
import json

from src.main2 import app

client = TestClient(app)

def test_get_root():
    response = client.get("/")
    assert response.status_code == 200
    # assert response.json() == {"status": "ok"}
    assert response.json() == {'greeting': 'Hello world'}


def test_post_train_and_evaluate():
    dict_df = pd.read_csv('./datasets/Iris.csv').to_dict(orient='list')
    response = client.post('http://fastapi:8886/train_and_evaluate', data=json.dumps({'df': dict_df}))
    assert response.status_code == 200
    data = response.json()
    assert 'results' in list(data.keys()) and 'filename' in list(data.keys())


def test_post_prediction():
    values = [random()*3.60+4.30, random()*2.40+2.00, random()*5.90+1.00, random()*2.40+0.10]
    response = client.post('http://fastapi:8886/prediction', data=json.dumps({'model_filename': './models/random_forest.sav', 'values': values}))
    assert response.status_code == 200
    result_class = response.json()['class']
    assert result_class == 'Iris-setosa' or result_class == 'Iris-versicolor' or result_class == 'Iris-virginica'


def test_prediction_pretrained_model_does_not_exists():
    values = [random()*3.60+4.30, random()*2.40+2.00, random()*5.90+1.00, random()*2.40+0.10]
    response = client.post('http://fastapi:8886/prediction', data=json.dumps({'model_filename': './models/fake_model_name.sav', 'values': values}))
    assert response.status_code == 200
    result_class = response.json()['class']
    assert result_class == 'Pre-trained model does not exists; train one before'