FROM python:3.10
WORKDIR ex_streamlit_fastapi

COPY requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY src ./src
CMD streamlit run src/main.py --server.port 8000
